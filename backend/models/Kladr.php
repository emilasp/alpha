<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kladr".
 *
 * @property string $name
 * @property string $socr
 * @property string $code
 * @property string $index
 * @property string $gninmb
 * @property string $uno
 * @property string $ocatd
 * @property string $status
 */
class Kladr extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kladr';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_kladr');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 40],
            [['socr'], 'string', 'max' => 10],
            [['code'], 'string', 'max' => 13],
            [['index'], 'string', 'max' => 6],
            [['gninmb', 'uno'], 'string', 'max' => 4],
            [['ocatd'], 'string', 'max' => 11],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('geo', 'Name'),
            'socr' => Yii::t('geo', 'Socr'),
            'code' => Yii::t('geo', 'Code'),
            'index' => Yii::t('geo', 'Index'),
            'gninmb' => Yii::t('geo', 'Gninmb'),
            'uno' => Yii::t('geo', 'Uno'),
            'ocatd' => Yii::t('geo', 'Ocatd'),
            'status' => Yii::t('geo', 'Status'),
        ];
    }
}
