<?php

/* @var $this \yii\web\View */
/* @var $content string */

use emilasp\geoapp\widgets\SelectCityWidget\SelectCityUserWidget;
use emilasp\im\frontend\widgets\ImCart\ImCart;
use emilasp\im\frontend\widgets\ImCatalog\ImSearch;
use emilasp\im\frontend\widgets\MenuBrandWidget\MenuBrandWidget;
use emilasp\site\common\extensions\FlashMsg\FlashMsg;
use emilasp\site\widgets\megamenu\Megamenu;
use emilasp\user\core\models\UserIssue;
use emilasp\user\core\widgets\UserIssueWidget\UserIssueWidget;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use emilasp\site\frontend\assets\AppAsset;
use emilasp\site\common\extensions\skins\Skins;
use emilasp\websocket\common\widgets\ws\WsConsoleWidget;

?>
<?php
$bundle = AppAsset::register($this);
$favPath = $bundle->baseUrl . '/images/favicon';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="<?= $favPath ?>/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= $favPath ?>/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $favPath ?>/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= $favPath ?>/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $favPath ?>/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $favPath ?>/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $favPath ?>/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $favPath ?>/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $favPath ?>/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= $favPath ?>/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $favPath ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= $favPath ?>/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $favPath ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= $favPath ?>/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= $favPath ?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Skins::widget(['theme' => Skins::THEME_ALPHA]) ?>

</head>
<body>
<?php $this->beginBody() ?>

<!--?= WsConsoleWidget::widget() ?-->

<?php FlashMsg::widget(); ?>

<?= UserIssueWidget::widget(['type' => UserIssue::TYPE_RECALL]) ?>
<?= UserIssueWidget::widget(['type' => UserIssue::TYPE_MESSAGE]) ?>


<header id="header" class="alpha">
    <div class="header-top">
        <div class="container top-block">
            <div>
                <a href="/" class="im-logo">&nbsp;</a>
            </div>
            <div class="header-top-right">
                <div class="row top-menu">
                    <div class="col-md-3"><i class="fa fa-home"></i>
                        <?= Yii::t('site', 'Select city') ?> :
                        <?= SelectCityUserWidget::widget(['name' => 'select-city']) ?>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-xs-5 text-right">
                                <i class="fa fa-truck"></i> <?= Yii::t('im', 'All Delivery') ?>
                            </div>
                            <div class="col-xs-4 text-right">
                                <i class="fa fa-thumbs-o-up "></i> <?= Yii::t('im', 'Return of goods') ?>
                            </div>
                            <div class="col-xs-3 text-right">
                                <i class="fa fa-star"></i> <?= Yii::t('im', 'Brand quality') ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-right">
                        <i class="fa fa-phone"></i>
                        <a data-remodal-target="user-issue-form-modal-1" class="phone-call">
                            <?= Yii::$app->getModule('im')->getSetting('im_phone') ?>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="im-header-search-block">
                            <?= ImSearch::widget() ?>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-4 white-list"><i class="fa fa-heart-o"></i> WhiteList</div>
                            <div class="col-md-6 text-right">
                                <?= ImCart::widget() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <?= Megamenu::widget(['menu' => 'im']) ?>
        </div>
    </div>

    <?= MenuBrandWidget::widget() ?>

</header>

<section id="wrapper" class="container">

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>


    <div class="main-container">
        <?php
        $mainColumns = 'fulls';
        if (!empty($this->params['sidebar']['left']) && !empty($this->params['sidebar']['right'])) {
            $mainColumns = '50 content-inner';
        } elseif (!empty($this->params['sidebar']['left']) || !empty($this->params['sidebar']['right'])) {
            $mainColumns = '66 content-inner';
        }
        ?>
        <?php if (!empty($this->params['sidebar']['left'])) : ?>
            <div class="sidebar-left">
                <?= $this->params['sidebar']['left'] ?>
            </div>
        <?php endif ?>

        <div class="width-<?= $mainColumns ?>">
            <?= $content ?>
        </div>

        <?php if (!empty($this->params['sidebar']['right'])) : ?>
            <div class="sidebar-right">
                <?= $this->params['sidebar']['right'] ?>
            </div>
        <?php endif ?>

    </div>

</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="copyrights col-sm-6 col-md-6">
                &copy; SPORT RUN <?= date('Y') ?>
            </div>
            <div class="social-icons col-sm-6 col-md-6 text-right">
                <?= Yii::$app->getModule('im')->getSetting('im_phone') ?>
            </div>
        </div>
    </div>
</footer>

<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter36815250 = new Ya.Metrika({
                    id: 36815250,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    ut: "noindex"
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks"); </script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/36815250?ut=noindex" style="position:absolute; left:-9999px;" alt=""/>
    </div>
</noscript> <!-- /Yandex.Metrika counter -->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

