<?php

/* @var $this \yii\web\View */
/* @var $content string */

use emilasp\geoapp\widgets\SelectCityWidget\SelectCityUserWidget;
use emilasp\im\frontend\widgets\ImCart\ImCart;
use emilasp\im\frontend\widgets\ImCatalog\ImSearch;
use emilasp\site\common\extensions\FlashMsg\FlashMsg;
use emilasp\site\widgets\megamenu\Megamenu;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use emilasp\site\frontend\assets\AppAsset;
use emilasp\site\common\extensions\skins\Skins;
use emilasp\websocket\common\widgets\ws\WsConsoleWidget;

?>

<?php AppAsset::register($this); ?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Skins::widget(['theme' => Skins::THEME_ALPHA]) ?>

</head>
<body>
<?php $this->beginBody() ?>

<!--?= WsConsoleWidget::widget() ?-->

<?php FlashMsg::widget(); ?>

<header id="header" class="alpha">
    <div class="header-top">
        <div class="container top-block">
            <div class="im-logo">
                <a href="/" class="">
                    <img src="/images/logo_alpha.png" height="80px">
                </a>
            </div>
            <div class="header-top-right">
                <div class="row top-menu">
                    <div class="col-md-3">
                        <i class="fa fa-phone"></i> <?= Yii::$app->getModule('im')->getSetting('im_phone') ?>
                    </div>
                    <div class="col-md-3"><i class="fa fa-home"></i>
                        <?= Yii::t('site', 'Select city')?> :
                        <?= SelectCityUserWidget::widget(['name' => 'seelct-city']) ?>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck"></i> <?= Yii::t('im', 'All Delivery') ?>
                            </div>
                            <div class="col-xs-4">
                                <i class="fa fa-thumbs-o-up "></i> <?= Yii::t('im', 'Return of goods') ?>
                            </div>
                            <div class="col-xs-4">
                                <i class="fa fa-star"></i> <?= Yii::t('im', 'Brand quality') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="im-header-search-block">
                            <?= ImSearch::widget() ?>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-5 white-list"><i class="fa fa-heart-o"></i>
                                <?= Yii::t('im', 'WhiteList') ?>
                            </div>
                            <div class="col-md-5 header-cart">
                                <?= ImCart::widget() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <?= Megamenu::widget(['menu' => 'im']) ?>
        </div>
    </div>

</header>

<section id="wrapper" class="container">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>


    <div class="main-container">
        <?php
        $mainColumns = 'full';
        if (!empty($this->params['sidebar']['left']) && !empty($this->params['sidebar']['right'])) {
            $mainColumns = '50';
        } elseif (!empty($this->params['sidebar']['left']) || !empty($this->params['sidebar']['right'])) {
            $mainColumns = '66';
        }
        ?>
        <?php if (!empty($this->params['sidebar']['left'])) : ?>
            <div class="sidebar-left">
                <?= $this->params['sidebar']['left'] ?>
            </div>
        <?php endif ?>

        <div class="content-inner width-<?= $mainColumns ?>">
            <?= $content ?>
        </div>

        <?php if (!empty($this->params['sidebar']['right'])) : ?>
            <div class="sidebar-right">
                <?= $this->params['sidebar']['right'] ?>
            </div>
        <?php endif ?>

    </div>

</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="copyrights col-sm-6 col-md-6">
                &copy; SPORT RUN <?= date('Y') ?>
            </div>
            <div class="social-icons col-sm-6 col-md-6 text-right">
                <?= Yii::$app->getModule('im')->getSetting('im_phone') ?>
            </div>
        </div>
    </div>
</footer>

<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter36815250 = new Ya.Metrika({ id:36815250, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, ut:"noindex" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/36815250?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

