<p>Пользователь отправил сообщение администратору сайта:</p>
<hr />
<p><strong>Заголовок сообщения:</strong> <?= Yii::t('site', $issue->title) ?></p>
<p><strong>Текст сообщения:</strong><br /> <?= $issue->text ?></p>
<p><strong>Телефон:</strong> <?= $issue->phone ?></p>
<p><strong>Email:</strong> <?= $issue->email ?></p>
<hr />
<p><strong>Info:</strong> <br /> <?= \yii\helpers\VarDumper::dumpAsString(json_decode($issue->info, true), 10,1) ?></p>

--<br />
