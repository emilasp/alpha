<?php
use emilasp\geoapp\models\GeoKladr;

$city = GeoKladr::findOne($order->city_id);
?>

<style>
    
</style>

<h2><?= Yii::t('im', 'Client text: new order') ?></h2>

<p><strong><?= Yii::t('im', 'Delivery') ?>:</strong> <?= $order->delivery->name ?></p>
<p><strong><?= Yii::t('im', 'Payment') ?>:</strong> <?= $order->payment->name ?></p>
<p><strong><?= Yii::t('site', 'Date') ?>:</strong> <?= $order->created_at ?></p>
<p><strong><?= Yii::t('site', 'Phone') ?>:</strong> <?= $order->client->phone ?></p>
<p><strong><?= Yii::t('site', 'City') ?>:</strong> <?= $city ? $city->name : '' ?></p>
<p><strong><?= Yii::t('site', 'Comment') ?>:</strong> <?= $order->comment_client ?></p>

<p>
    <?= $orderTable ?>
</p>
