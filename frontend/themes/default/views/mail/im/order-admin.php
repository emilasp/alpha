<?php
use emilasp\geoapp\models\GeoKladr;
use emilasp\geoapp\models\GeoStreet;

$city = GeoKladr::findOne($order->city_id);

$address = json_decode($order->address, true);

$strAddress = '';
if (is_array($address)) {
    $address = $address[0];
    /*if (!empty($address['street'])) {
        $street            = GeoStreet::findOne($address['street']);
        $address['street'] = $street->name;
    }*/
    if (!empty($address['city']) && $city) {
        $address['city'] = $city->name;
    }

    $arrAddr = [];
    foreach ($address as $fieldName => $itemAddress) {
        if ($itemAddress) {
            $arrAddr[] = '<strong>' . Yii::t('im', $fieldName) . ':</strong> ' . $itemAddress;
        }
    }
    $strAddress = implode('<br />', $arrAddr);
}


?>
<h2><?= Yii::t('im', 'Admin text: new order') ?></h2>

<p><strong><?= Yii::t('im', 'Delivery') ?>:</strong> <?= $order->delivery->name ?></p>
<p><strong><?= Yii::t('im', 'Payment') ?>:</strong> <?= $order->payment->name ?></p>
<p><strong><?= Yii::t('site', 'Date') ?>:</strong> <?= $order->created_at ?></p>
<p><strong><?= Yii::t('site', 'Fio') ?>:</strong> <?= $order->client->fio ?></p>
<p><strong><?= Yii::t('site', 'Phone') ?>:</strong> <?= $order->client->phone ?></p>
<p><strong><?= Yii::t('site', 'City') ?>:</strong> <?= $city ? $city->name : '' ?></p>
<p><strong><?= Yii::t('site', 'Address') ?>:</strong> <br /> <?= $strAddress ?></p>
<p><strong><?= Yii::t('site', 'Comment') ?>:</strong> <?= $order->comment_client ?></p>

<p>
    <?= $orderTable ?>
</p>
