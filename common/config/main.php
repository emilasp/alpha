<?php
use emilasp\imports\imports\TestConf;
use emilasp\imports\ImportsModule;
use emilasp\files\models\File;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules'    => [
        'variety' => [
            'class' => 'emilasp\variety\VarietyModule',
        ],
        'json'    => [
            'class' => 'emilasp\json\JsonModule',
        ],
        'geo'     => [
            'class' => 'emilasp\geoapp\GeoModule',
        ],
        'files'   => [
            'class'  => 'emilasp\files\FileModule',
            'config' => [
                'watermarkSrc'   => '@common/media/images/watermark.png',
                'scaleWatermark' => 5,
                'quality'        => 100,
                'noImage'        => [
                    'path' => '/system/noImage/',
                    'file' => 'no-image.png',
                ],
            ],
            'sizes'  => [
                'default'                                      => [
                    File::SIZE_ICO => ['size' => '100x100', 'watermark' => false, 'crop' => true],
                    File::SIZE_MIN => ['size' => '150x120', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MID => ['size' => '250x200', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MED => ['size' => '400x360', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MAX => ['size' => '800x600', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                ],
                \emilasp\im\common\models\Product::className() => [
                    File::SIZE_ICO => ['size' => '100x100', 'watermark' => false, 'crop' => true],
                    File::SIZE_MIN => ['size' => '96x144', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MID => ['size' => '218x320', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MED => ['size' => '400x600', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MAX => ['size' => '800x600', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                ],

                \emilasp\im\common\models\Brand::className() => [
                    'image_id' => [
                        File::SIZE_ICO => ['size' => '100x100', 'watermark' => false, 'crop' => true],
                        File::SIZE_MIN => ['size' => '150x150', 'watermark' => false, 'aspectRatio' => true],
                        File::SIZE_MID => ['size' => '250x250', 'watermark' => false, 'aspectRatio' => true],
                        File::SIZE_MED => ['size' => '600x400', 'watermark' => false, 'aspectRatio' => true],
                        File::SIZE_MAX => ['size' => '800x600', 'watermark' => false, 'aspectRatio' => true],
                        File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                    ],
                    'banners'  => [
                        File::SIZE_MAX => ['size' => '1130x', 'watermark' => false, 'aspectRatio' => true],
                        File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                    ],
                ],

                /*\emilasp\taxonomy\models\Category::className() => [
                    'min_' => ['size' => '100x100','watermark' => true,'crop' => true]
                ]*/
            ],
        ],
        'imports' => [
            'class'   => ImportsModule::className(),
            'imports' => [
                TestConf::className(),
            ],
        ],
    ],
    'components' => [
        'db'       => require(__DIR__ . '/include/db.php'),
        'db_kladr' => require(__DIR__ . '/include/db-kladr.php'),
        'rabbit'   => require(__DIR__ . '/include/rabbit.php'),
        'redis'    => require(__DIR__ . '/include/redis.php'),
        'sphinx'   => require(__DIR__ . '/include/sphinx.php'),
        'mail'     => require(__DIR__ . '/include/mail.php'),

        'sender' => [
            'class' => \emilasp\core\components\SenderComponent::className(),
            'email' => [
                'admin'     => 'manager@sport-run.ru',
                'adminName' => 'sport-run.ru',
                'support'   => 'manager@sport-run.ru',
            ],
        ],


        'websocket' => [
            'class'   => 'emilasp\websocket\common\components\WsConnect',
            'servers' => [
                'websocket' => [
                    'class'       => 'emilasp\websocket\common\components\WsHandlerDaemon',
                    'pid'         => '/tmp/websocket.p:qid',
                    'websocket'   => 'tcp://0.0.0.0:9001',
                    'localsocket' => 'tcp://workers:9002',
                    //'master' => 'tcp://127.0.0.1:9000',
                    //'eventDriver' => 'event'
                ],
            ],
        ],


        'session' => [
            'class' => 'yii\redis\Session',
            'redis' => [
                'hostname' => 'redis',
                'port'     => 6379,
                'database' => 2,
                /*'cookieParams' => [
                    'httponly' => true,
                    'lifetime' => 3600243012
                ],*/
                //'timeout' => 3600243012,
                //'useCookies' => true,
            ],
        ],

        'cache'  => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => 'redis',
                'port'     => 6379,
                'database' => 5,
            ],
        ],
        'access' => [
            'class' => 'yii\web\AccessControl',
        ],
        'i18n'   => [
            'translations' => [
                'variety'     => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-variety/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'variety' => 'variety.php',
                    ],
                ],
                'settings'    => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-settings/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'settings' => 'settings.php',
                    ],
                ],
                'taxonomy'    => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-taxonomy/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'taxonomy' => 'taxonomy.php',
                    ],
                ],
                'files'       => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-files/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'files' => 'files.php',
                    ],
                ],
                'site'        => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-site/common/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'site' => 'site.php',
                    ],
                ],
                'seo'         => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-seo/common/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'seo' => 'seo.php',
                    ],
                ],
                'json'        => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-json/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'json' => 'json.php',
                    ],
                ],
                'im'          => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-im/common/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'im' => 'im.php',
                    ],
                ],
                'user'        => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-user/core/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'user' => 'user.php',
                    ],
                ],
                'userbackend' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-user/backend/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'userbackend' => 'user-backend.php',
                    ],
                ],
                'adv'         => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-adv/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'adv' => 'adv.php',
                    ],
                ],
                'cpa'         => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-cpa/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'cpa' => 'cpa.php',
                    ],
                ],
                'landing'         => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-landing/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'landing' => 'landing.php',
                    ],
                ],
                'goal'        => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@vendor/emilasp/yii2-goal/common/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap'        => [
                        'goal' => 'goal.php',
                    ],
                ],
            ],
        ],
        'log'    => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                /*[
                    'class'          => 'emilasp\core\components\ErrorEmailTarget',
                    'mailComponent'  => 'mail',
                    'levels'         => ['error'],
                    //'categories' => ['yii\db\*'],
                    'message'        => [
                        'from'    => ['noreply@amulex.ru'],
                        'to'      => ['fernando@amulex.ru', 'simonyan@amulex.ru'],
                        'subject' => 'Ошибки на сайте sale.amulex.ru',
                    ],
                    'exceptCategory' => [
                        'application',
                        'HttpException:404',
                        'HttpException:403',
                    ],
                ],*/
            ],
        ],
    ],
];
