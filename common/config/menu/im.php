<?php
use emilasp\im\common\models\Brand;
use emilasp\im\common\models\ImCategory;
use emilasp\im\common\models\ProductSphinx;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>

<?php

$forMen   = ImCategory::find()->byStatus()->where(['id' => 3])->one();
$forWomen = ImCategory::find()->byStatus()->where(['id' => 4])->one();

$brands = Brand::find()->byStatus()->orderBy('name')->all();

?>


<div class="navbar yamm">

    <!--div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            <a href="/" class="navbar-brand"><?= Yii::t('im', 'Sport Shoes') ?></a>
        </div-->
    <div id="navbar-collapse-top" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <!-- Classic list -->
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"
                   aria-expanded="false"><?= Yii::t('im', 'Men') ?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <div class="yamm-content">

                            <?php
                            $menFacets   = ProductSphinx::getSphinxFacets(null, null, [], [], [], ['sex' => [1, 3]]);
                            $womenFacets = ProductSphinx::getSphinxFacets(null, null, [], [], [], ['sex' => [2, 3]]);
                            $brandMen    = ArrayHelper::map($menFacets['facets']['brand_id'], 'brand_id', 'count');
                            $brandWomen  = ArrayHelper::map($womenFacets['facets']['brand_id'], 'brand_id', 'count');

                            $menFacets   = ProductSphinx::getSphinxCategoryAllFacet(['sex' => [1, 3]]);
                            $womenFacets = ProductSphinx::getSphinxCategoryAllFacet(['sex' => [2, 3]]);
                            ?>

                            <div class="float-left">
                                <h4><?= Yii::t('im', 'Men shoes') ?></h4>

                                <div class="top-menu-categories">
                                    <?php foreach ($forMen->children()->orderBy('name')->all() as $category) : ?>
                                        <?php if (isset($menFacets[$category->id])) : ?>
                                            <div class="top-menu-category">
                                                <a href="<?= $category->getUrl() ?>">
                                                    <?= $category->name ?>
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                    <hr/>
                                    <div class="top-menu-category">
                                        <a href="<?= $forMen->getUrl() ?>">
                                            <?= Yii::t('im', 'All category') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="float-left ">
                                <h4><?= Yii::t('im', 'Brands') ?></h4>

                                <div class="top-menu-brands">
                                    <?php foreach ($brands as $brand) : ?>
                                        <?php if (isset($brandMen[$brand->id])) : ?>
                                            <div class="top-menu-brand">
                                                <a href="<?= $brand->getUrl() ?>">
                                                    <?= $brand->name ?>
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"
                   aria-expanded="false"><?= Yii::t('im', 'Women') ?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <div class="yamm-content">

                            <div class="float-left">
                                <h4><?= Yii::t('im', 'Women shoes') ?></h4>

                                <div class="top-menu-categories">
                                    <?php foreach ($forWomen->children()->orderBy('name')->all() as $category) : ?>
                                        <?php if (isset($womenFacets[$category->id])) : ?>
                                            <div class="top-menu-category">
                                                <a href="<?= $category->getUrl() ?>">
                                                    <?= $category->name ?>
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                    <hr/>
                                    <div class="top-menu-category">
                                        <a href="<?= $forWomen->getUrl() ?>">
                                            <?= Yii::t('im', 'All category') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="float-left ">
                                <h4><?= Yii::t('im', 'Brands') ?></h4>

                                <div class="top-menu-brands">
                                    <?php foreach ($brands as $brand) : ?>
                                        <?php if (isset($brandWomen[$brand->id])) : ?>
                                            <div class="top-menu-brand">
                                                <a href="<?= $brand->getUrl() ?>">
                                                    <?= $brand->name ?>
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li><a href="<?= Url::toRoute('/im/page/delivery') ?>"><?= Yii::t('im', 'Delivery and Payment') ?></a></li>
            <!--li><a href="<?= Url::toRoute('/im/payment') ?>"><?= Yii::t('im', 'Payment') ?></a></li-->
            <li><a href="<?= Url::toRoute('/im/page/sizes') ?>"><?= Yii::t('im', 'Sizes') ?></a></li>

            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"
                   aria-expanded="false"><?= Yii::t('im', 'Akcii') ?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <div class="yamm-content">

                                <h4><?= Yii::t('im', 'Akcii now') ?></h4>
                            
                            <a class="red-link" href="<?= Url::toRoute('/im/page/akcii-leto') ?>">
                                <?= Yii::t('im', 'Akciya leto') ?>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            
            <li><a href="<?= Url::toRoute('/im/page/contacts') ?>"><?= Yii::t('site', 'Contacts') ?></a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php if (Yii::$app->user->isGuest) : ?>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"
                       aria-expanded="false"><?= Yii::t('user', 'Login') ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="<?= Url::toRoute(['/user/login']) ?>">
                                            <?= Yii::t('user', 'Login') ?>
                                        </a><br/>
                                        <a href="<?= Url::toRoute(['/user/registration']) ?>">
                                            <?= Yii::t('user', 'Registration') ?>
                                        </a>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            <?php else : ?>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">
                        <i class="fa fa-user"></i> <?= Yii::$app->user->identity->email ?><b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="<?= Url::toRoute(['/user/profile']) ?>">
                                            <?= Yii::t('user', 'Profile') ?>
                                        </a><br/>
                                        <a href="<?= Url::toRoute(['/user/login/logout']) ?>">
                                            <?= Yii::t('user', 'Logout') ?>
                                        </a>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            <?php endif ?>
        </ul>
    </div>

</div>